import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import {CommonModule} from '@angular/common';


const routes: Routes = [
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: 'login',
    component: LoginComponent,
    data: {}
  },
  { path: 'register',
    component: RegisterComponent,
    data: {}
  },
  {
    path: 'user',
    loadChildren: () => import('./main-routing.module').then( m => m.MainRoutingModule),
    // canActivate: [ AuthGuardService ],
    data: {}
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
      // { enableTracing: true }),
    ReactiveFormsModule,
    FormsModule,
    CommonModule
  ],
  exports: [RouterModule],
  declarations: [
    LoginComponent,
    RegisterComponent
  ]
})
export class AppRoutingModule { }
