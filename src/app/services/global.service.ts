import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { UserInfo } from '../interfaces/user-info';
import { Connection } from '../interfaces/connection';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  // Config variable(s)
  // private server = 'http://80.93.123.134:6061';
  private server = 'http://fxaio.com:6061';

  // Data variable(s)

  private hash$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private role$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private nick$: BehaviorSubject<string> = new BehaviorSubject<string>('');

  private hash: string;
  private role: string;
  private nick: string;

  constructor( private http: HttpClient,
               private router: Router
  ) {
    this.hash$.asObservable().subscribe(data => this.hash = data);
    this.role$.asObservable().subscribe(data => this.role = data);
    this.nick$.asObservable().subscribe(data => this.nick = data);
  }

  getHash(): string {
    return this.hash;
  }

  getRole(): string {
    return this.role;
  }

  getNick(): string {
    return this.nick;
  }

  login(data: any): Observable<any> {
    const body = new HttpParams()
      .set('login', data.username)
      .set('password', data.password);

    return this.http.post(this.server + '/login',
      body.toString(),
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
      }
    ).pipe(
      map(answ => {
        if ( answ.hasOwnProperty('Hash') && answ.hasOwnProperty('role') ) {
          // @ts-ignore
          this.hash$.next(answ.Hash);
          // @ts-ignore
          this.role$.next(answ.role);
          // @ts-ignore
          this.nick$.next(data.username);
          this.router.navigate(['/user/profile']);
          return Object.assign(answ, {error: false});
        } else {
          return Object.assign(answ, {error: true});
        }
      })
    );
  }

  register(data: any): Observable<any> {
    const body = new HttpParams()
      .set('login', data.username)
      .set('password', data.password)
      .set('role', data.role)
      .set('parent', data.parent);

    return this.http.post(this.server + '/registeruser',
      body.toString(),
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
      }
    );
  }


  logout(): Observable<any> {
    const reqOption = new HttpParams()
      .set('Hash', this.hash);

    return this.http.post<any>(this.server + '/logout',
      reqOption.toString(),
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
      }
    ).pipe(
      tap(() => this.router.navigate(['/login']))
    );
  }

  getUserInfo(): Observable<UserInfo> {
    return this.http.post(this.server + '/profile/get', JSON.stringify({Hash: this.hash})).pipe(
      tap((answ: UserInfo) => console.log('INFO answ: ', answ))
    );
  }

  updateUserInfo(data: UserInfo): Observable<UserInfo> {
    return this.http.post<UserInfo>( this.server + '/profile/update', JSON.stringify(Object.assign({Hash: this.hash}, data)));
  }


  getUsers(): Observable<Array<any>> {
    const reqOption = new HttpParams()
      .set('Hash', this.hash);

    return this.http.post<any>(this.server + '/getusers',
      reqOption.toString(),
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
      });
  }

  updateConnection( connection: Connection, UserLogin?: string ): Observable<Connection> {
    let data = {
      Hash: this.hash,
      connection
    };
    if( UserLogin ) {
      data = Object.assign(data, {UserLogin});
    }
    return this.http.post<Connection>( this.server + '/connection/update', JSON.stringify( data ));
  }

  getAllConnections( provider?: string ): Observable<Array<Connection>> {
    let data = { Hash: this.hash };
    if ( provider ){
      data = Object.assign(data, {UserLogin: provider});
    }
    return this.http.post<Array<Connection>>(this.server + '/connection/getall',
      JSON.stringify( data ));
  }
}
