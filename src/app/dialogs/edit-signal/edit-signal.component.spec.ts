import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSignalComponent } from './edit-signal.component';

describe('EditSignalComponent', () => {
  let component: EditSignalComponent;
  let fixture: ComponentFixture<EditSignalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSignalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSignalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
