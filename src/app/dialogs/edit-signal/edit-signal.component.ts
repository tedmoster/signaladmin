import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-edit-signal',
  templateUrl: './edit-signal.component.html',
  styleUrls: ['./edit-signal.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class EditSignalComponent implements OnInit {

  public editForm: FormGroup;

  constructor( private globalService: GlobalService,
               public modal: NgbActiveModal,
               private formBuilder: FormBuilder
  ) {
    this.editForm = formBuilder.group({
      enabled: [Boolean, [Validators.required]],
      signalName: ['', [Validators.pattern(/^\S+$/), Validators.required]]
    });

    // this.globalService.getEdigFormData().subscribe( data => {
    //   this.editForm.controls.enabled.setValue( data.enabled );
    //   this.editForm.controls.signalName.setValue( data.signalName );
    // });
  }

  ngOnInit() {
    // console.log('Hash: ', this.globalService.getHash());
    // console.log('Role: ', this.globalService.getRole());
    // console.log('Login: ', this.globalService.getNick());
  }

}
