import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class RegisterDialogComponent implements OnInit {

  public registerForm: FormGroup;

  constructor( private globalService: GlobalService,
               public modal: NgbActiveModal,
               private formBuilder: FormBuilder
  ) {
    this.registerForm = formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.pattern(/^\S+$/), Validators.required]]
    });
  }

  ngOnInit() {
    console.log('Hash: ', this.globalService.getHash());
    console.log('Role: ', this.globalService.getRole());
    console.log('Login: ', this.globalService.getNick());
  }

  go(value) {
    this.modal.close({
      status: true,
      data: Object.assign({}, value)
    });
  }

}
