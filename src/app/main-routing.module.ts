import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {CommonModule} from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RegisterDialogComponent } from './dialogs/register-dialog/register-dialog.component';
import { EditSignalComponent } from './dialogs/edit-signal/edit-signal.component';

import { UserComponent } from './components/user/user.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TradersComponent } from './components/traders/traders.component';
import { ProvidersComponent } from './components/providers/providers.component';
import { ConnectionsComponent } from './components/connections/connections.component';


const routes: Routes = [
  { path: '',
    component: SidebarComponent,
    children: [
      {
        path: 'profile',
        component: UserComponent,
        data: {}
      }, {
        path: 'traders',
        component: TradersComponent,
        data: {}
      }, {
        path: 'providers',
        component: ProvidersComponent,
        data: {}
      }, {
        path: 'connections',
        component: ConnectionsComponent,
        data: {}
      }, {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
      }
    ]
  },

];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    FontAwesomeModule,
    NgbModule
  ],
  exports: [RouterModule],
  declarations: [
    UserComponent,
    SidebarComponent,
    TradersComponent,
    ProvidersComponent,
    EditSignalComponent,
    RegisterDialogComponent,
    ConnectionsComponent
  ],
  entryComponents: [ EditSignalComponent, RegisterDialogComponent ]
})
export class MainRoutingModule { }
