export interface Connection {
  host: string;
  port: number;
  login: number;
  name: string;
  active: boolean;
  password: string;
  signalName: string;
  allowFollow: boolean;
}
