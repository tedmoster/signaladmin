export interface UserInfo {
  login: string;
  name: string;
  type: string;
  urlName: string;
}
