import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faUser } from '@fortawesome/free-solid-svg-icons';


import { GlobalService } from '../../services/global.service';
import {distinctUntilChanged} from 'rxjs/operators';

import { UserInfo } from '../../interfaces/user-info';
import {conditionallyCreateMapObjectLiteral} from '@angular/compiler/src/render3/view/util';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  // ICONS
  faUserAlt = faUser;

  public profileForm: FormGroup;
  public formIsChanged = false;

  constructor( private globalService: GlobalService,
               private formBuilder: FormBuilder
  ) {
    this.profileForm = this.formBuilder.group({
      login: ['', [Validators.required, Validators.pattern(/^\w+$/)]],
      name: ['', [Validators.required, Validators.minLength(2)]],
      type: ['0', [Validators.required]],
      urlName: ['', [Validators.pattern(/^\w+$/)]]
    });

    this.profileForm.valueChanges.pipe(
      distinctUntilChanged()
    ).subscribe(data => this.formIsChanged = true);
  }

  ngOnInit() {
    this.globalService.getUserInfo().subscribe(data => {
      this.profileForm.controls.login.setValue(data.login);
      this.profileForm.controls.name.setValue(data.name);
      this.profileForm.controls.type.setValue(data.type);
      this.profileForm.controls.urlName.setValue(data.urlName);
    });

    this.profileForm.controls.type.valueChanges.subscribe(() => this.profileForm.controls.type.markAsTouched());
  }

  submitChanges() {
    this.globalService.updateUserInfo(this.profileForm.value);
  }

}
