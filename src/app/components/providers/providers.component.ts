import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormArray, Validators, AbstractControl} from '@angular/forms';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import {NgbModal, ModalDismissReasons, NgbModalOptions, NgbTabChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import {faUserPlus, faTimesCircle, faNetworkWired, faAngleLeft, faWifi, faServer, faEdit} from '@fortawesome/free-solid-svg-icons';

import { GlobalService } from '../../services/global.service';

import { RegisterDialogComponent } from '../../dialogs/register-dialog/register-dialog.component';

import {Connection} from '../../interfaces/connection';

@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.scss']
})
export class ProvidersComponent implements OnInit {

  // ICONS
  providersIcon = faNetworkWired;
  addProviderIcon = faUserPlus;
  removeProviderIcon = faTimesCircle;
  backIcon = faAngleLeft;
  signalIcon = faWifi;
  connectionIcon = faServer;
  editIcon = faEdit;

  // Property(ies)
  listView = true;
  isEmptyList = false;
  signalProviderLogin = '';
  connectionsForm: FormGroup;
  $providers: Observable<Array<any>> = new Observable<Array<any>>();

  modalOptions: NgbModalOptions = {
    backdrop: 'static',
    centered: true
  };


  constructor( private globalService: GlobalService,
               private modalService: NgbModal,
               private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    if ( this.listView ) {
      this.updateData();
    }

    this.connectionsForm = this.formBuilder.group({
      amount: [{value: null}],
      connections: new FormArray([])
    });
  }

  get connectionsFormControl() {return this.connectionsForm.controls; }
  get connectionsFormArray() { return this.connectionsFormControl.connections as FormArray; }

  changeSignalProvider( login: string ) {
    this.isEmptyList = false;
    this.signalProviderLogin = login;
    this.switchView();
    this.globalService.getAllConnections( this.signalProviderLogin ).subscribe(data => {
      if ( Array.isArray(data) ) {
        this.connectionsForm.controls.amount.setValue(data.length);
        for ( const connection of data ) {
          const fg = this.formBuilder.group({
            host: [connection.host],
            port: [connection.port],
            login: [connection.login],
            signalName: [connection.signalName, [Validators.required]],
            password: [connection.password, [Validators.pattern(/^.{3,16}$/), Validators.required]],
            active: [connection.active, [Validators.required]],
            allowFollow: [connection.allowFollow, [Validators.required]],
            name: [connection.name]
          });

          fg.controls.host.disable();
          fg.controls.port.disable();
          fg.controls.login.disable();
          fg.controls.name.disable();

          this.connectionsFormArray.push(fg);
        }
      } else {
        this.isEmptyList = true;
      }
    });
  }

  switchView() {
    this.listView = !this.listView;
  }

  private updateData() {
    this.$providers = this.globalService.getUsers()
      .pipe(
        map( data => data.filter( elem => elem.role === 'signalProvider'))
      );
  }

  openDialog() {
    this.modalService.open(RegisterDialogComponent, this.modalOptions).result
      .then((answ) => {
        if ( answ.status ) {
          this.globalService.register(Object.assign(answ.data, {role: 'signalProvider', parent: this.globalService.getNick()}))
            .subscribe(reg => {
              console.log('Register answ: ', reg);
              this.updateData();
            });
        }
      }).catch( err => console.log('Modal window err: ', err));
  }

  updateConnection(data: Connection, i: number) {
    this.connectionsFormArray[i].controls.host.disable();
    this.connectionsFormArray[i].controls.port.disable();
    this.connectionsFormArray[i].controls.login.disable();
    this.connectionsFormArray[i].controls.name.disable();
    this.globalService.updateConnection( data, this.signalProviderLogin );
  }

}
