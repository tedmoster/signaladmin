import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private globalService: GlobalService
  ) {
    this.loginForm = formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.pattern(/^\S+$/), Validators.required]]
    });
  }

  ngOnInit() {
  }

  go(value) {
    console.log('Form value: ', value);
    this.globalService.login(value).subscribe(
      (answer) => console.log('Answer: ', answer),
      ( err ) => console.log('Error: ', err)
    );
  }
}
