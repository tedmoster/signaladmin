import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private globalService: GlobalService) {
    this.registerForm = formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.pattern(/^\S+$/), Validators.required]],
      role: ['0', [Validators.required, Validators.minLength(2)]]
    });
  }

  ngOnInit() {
  }

  go(value) {
    console.log('Form value: ', value);
    this.globalService.register(value).subscribe(res => {
      console.log('Register answ: ', res);
      this.globalService.login({username: value.username, password: value.password}).subscribe((answ) => {
        console.log('Login after register: ', answ);
      });
    });
  }

}
