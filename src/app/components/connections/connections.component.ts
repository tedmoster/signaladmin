import { Component, OnInit } from '@angular/core';
import {faUsers} from '@fortawesome/free-solid-svg-icons';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../../services/global.service';
import {Connection} from '../../interfaces/connection';

@Component({
  selector: 'app-connections',
  templateUrl: './connections.component.html',
  styleUrls: ['./connections.component.scss']
})
export class ConnectionsComponent implements OnInit {
  // ICONS
  tradersIcon = faUsers;

  // Properties
  connectionsAmount = 0;
  isEmptyConnectionList = false;
  connectionsForm: FormGroup;

  constructor( private globalService: GlobalService,
               private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.connectionsForm = this.formBuilder.group({
      amount: [{value: null}],
      connections: new FormArray([])
    });

    this.getAllConnections();
  }


  get connectionsFormControl() {return this.connectionsForm.controls; }
  get connectionsFormArray() { return this.connectionsFormControl.connections as FormArray; }

  getAllConnections(  ) {
    this.isEmptyConnectionList = false;
    this.globalService.getAllConnections().subscribe(data => {
      if ( Array.isArray(data) ) {
        this.connectionsForm.controls.amount.setValue(data.length);
        for ( const connection of data ) {
          const fg = this.formBuilder.group({
            host: [connection.host],
            port: [connection.port],
            login: [connection.login],
            signalName: [connection.signalName, [Validators.required]],
            password: [connection.password, [Validators.pattern(/^.{3,16}$/), Validators.required]],
            active: [connection.active, [Validators.required]],
            allowFollow: [connection.allowFollow, [Validators.required]],
            name: [connection.name]
          });

          fg.controls.host.disable();
          fg.controls.port.disable();
          fg.controls.login.disable();
          fg.controls.name.disable();

          this.connectionsFormArray.push(fg);
        }
      } else {
        this.isEmptyConnectionList = true;
      }
    });
  }

  updateConnection(data: Connection, i: number) {
    // @ts-ignore
    this.connectionsFormArray.controls[i].controls.host.disable();
    // @ts-ignore
    this.connectionsFormArray.controls[i].controls.port.disable();
    // @ts-ignore
    this.connectionsFormArray.controls[i].controls.login.disable();
    // @ts-ignore
    this.connectionsFormArray.controls[i].controls.name.disable();
    this.globalService.updateConnection( data ).subscribe(res => console.log);
  }
}
