import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {NgbModal, ModalDismissReasons, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {faUsers, faUserPlus, faTimesCircle, faAngleLeft} from '@fortawesome/free-solid-svg-icons';

import { GlobalService } from '../../services/global.service';

import { RegisterDialogComponent } from '../../dialogs/register-dialog/register-dialog.component';

import { Connection } from '../../interfaces/connection';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-traders',
  templateUrl: './traders.component.html',
  styleUrls: ['./traders.component.scss']
})
export class TradersComponent implements OnInit {

  // ICONS
  tradersIcon = faUsers;
  addTraderIcon = faUserPlus;
  removeTraderIcon = faTimesCircle;
  backIcon = faAngleLeft;

  // Property(ies)
  isListView = true;
  isEmptyConnectionList = false;
  currentTrader = '';
  connectionsForm: FormGroup;
  $data: Observable<Array<any>> = new Observable<Array<any>>();
  modalOptions: NgbModalOptions = {
    backdrop: 'static',
    centered: true
  };

  constructor( private globalService: GlobalService,
               private modalService: NgbModal,
               private formBuilder: FormBuilder ) { }

  ngOnInit() {
    if ( this.isListView ) {
      this.updateData();
    }

    this.connectionsForm = this.formBuilder.group({
      amount: [{value: null}],
      connections: new FormArray([])
    });
  }

  private updateData() {
    this.$data = this.globalService.getUsers()
      .pipe(
        map( data => data.filter( elem => elem.role === 'trader'))
      );
  }

  openDialog() {
    this.modalService.open(RegisterDialogComponent, this.modalOptions).result
      .then((answ) => {
      if ( answ.status ) {
        this.globalService.register(Object.assign(answ.data, {role: 'trader', parent: this.globalService.getNick()}))
          .subscribe(reg => {
            console.log('Register answ: ', reg);
            this.updateData();
          });
      }
    }).catch( err => console.log('Modal window err: ', err));
  }

  switchView() {
    this.isListView = !this.isListView;
  }

  get connectionsFormControl() {return this.connectionsForm.controls; }
  get connectionsFormArray() { return this.connectionsFormControl.connections as FormArray; }

  changeTrader( login: string ) {
    this.currentTrader = login;
    this.isEmptyConnectionList = false;
    this.switchView();
    this.globalService.getAllConnections( this.currentTrader ).subscribe(data => {
      if ( Array.isArray(data) ) {
        this.connectionsForm.controls.amount.setValue(data.length);
        for ( const connection of data ) {
          const fg = this.formBuilder.group({
            host: [connection.host],
            port: [connection.port],
            login: [connection.login],
            signalName: [connection.signalName, [Validators.required]],
            password: [connection.password, [Validators.pattern(/^.{3,16}$/), Validators.required]],
            active: [connection.active, [Validators.required]],
            allowFollow: [connection.allowFollow, [Validators.required]],
            name: [connection.name]
          });

          fg.controls.host.disable();
          fg.controls.port.disable();
          fg.controls.login.disable();
          fg.controls.name.disable();

          this.connectionsFormArray.push(fg);
        }
      } else {
        this.isEmptyConnectionList = true;
      }
    });
  }


  updateConnection(data: Connection, i: number) {
    this.connectionsFormArray[i].controls.host.disable();
    this.connectionsFormArray[i].controls.port.disable();
    this.connectionsFormArray[i].controls.login.disable();
    this.connectionsFormArray[i].controls.name.disable();
    this.globalService.updateConnection( data, this.currentTrader );
  }
}
