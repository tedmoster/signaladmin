import { Component, OnInit } from '@angular/core';
import {faUsers, faNetworkWired, faUserAlt, faSignOutAlt, faServer} from '@fortawesome/free-solid-svg-icons';

import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  // ICONS
  tradersIcon = faUsers;
  providersIcon = faNetworkWired;
  profileIcon = faUserAlt;
  logoutIcon = faSignOutAlt;
  connectionIcon = faServer;


  constructor( private globalService: GlobalService ) { }

  ngOnInit() {
  }

  logout() {
    this.globalService.logout().subscribe(data => {
      console.log('LogOut: ', data);
    });
  }

}
